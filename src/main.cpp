#include <vector>
#include <cstring>
#include <cassert>
#include <cmath>
#include <iostream>

#ifdef NDEBUG
constexpr bool enableValidationLayers = false;
#else
constexpr bool enableValidationLayers = true;
#endif

// uncomment to use host coherent memory for fractal storage
//#define USE_HOST_COHERENT_MEM

#include "vk_utils.h"
#include "vk_images.h"
#include "vk_buffers.h"
#include "vk_copy.h"
#include "vk_alloc.h"
#include "vk_descriptor_sets.h"
#include "vk_pipeline.h"

#include "../shaders/shaderCommon.h"
#include "Bitmap.h"

class ComputeApplication
{
private:
  struct Pixel
  {
      float r, g, b, a;
  };

  bool m_enableValidation = enableValidationLayers;
  std::vector<const char*> m_validationLayers;

  VkInstance       m_instance       = VK_NULL_HANDLE;
  VkCommandPool    m_commandPool    = VK_NULL_HANDLE;
  VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
  VkDevice         m_device         = VK_NULL_HANDLE;
  VkQueue          m_queue          = VK_NULL_HANDLE;

  vk_utils::QueueFID_T m_queueFamilyIDXs {UINT32_MAX, UINT32_MAX, UINT32_MAX};

  VkPipeline       m_pipeline       = VK_NULL_HANDLE;
  VkPipelineLayout m_pipelineLayout = VK_NULL_HANDLE;

  VkDescriptorSet       m_dSet       = VK_NULL_HANDLE;
  VkDescriptorSetLayout m_dSetLayout = VK_NULL_HANDLE;
  std::shared_ptr<vk_utils::DescriptorMaker> m_pBindings = nullptr;

  static constexpr uint64_t scratchMemSize = 16 * 16 * 1024u;
  std::shared_ptr<vk_utils::IMemoryAlloc> m_pAlloc = nullptr;
  std::shared_ptr<vk_utils::ICopyEngine>  m_pCopy  = nullptr;

  void * m_pDeviceFeatures = nullptr;
  VkPhysicalDeviceFeatures m_enabledDeviceFeatures = {};
  std::vector<const char*> m_deviceExtensions      = {};
  std::vector<const char*> m_instanceExtensions    = {};

  size_t m_fractalBufSize  = 0;
  VkBuffer m_fractalBuffer = VK_NULL_HANDLE;
  uint32_t m_fractalBufferAllocID = -1;

  uint32_t m_texAlloc = -1;
  std::vector<VkImage>     m_images;
  std::vector<VkImageView> m_imageViews;
  std::vector<VkSampler>   m_samplers;

  VkDebugReportCallbackEXT m_debugReportCallback = nullptr;

  // ***********************************************************************

  void InitVulkan(uint8_t a_deviceId = 0)
  {
    m_validationLayers.push_back("VK_LAYER_KHRONOS_validation");
    VK_CHECK_RESULT(volkInitialize());

    VkApplicationInfo appInfo = {};
    appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pNext              = nullptr;
    appInfo.pApplicationName   = "Bindless sample";
    appInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
    appInfo.pEngineName        = "None";
    appInfo.engineVersion      = VK_MAKE_VERSION(0, 1, 0);
    appInfo.apiVersion         = VK_MAKE_VERSION(1, 1, 0);

    m_instance = vk_utils::createInstance(m_enableValidation, m_validationLayers, m_instanceExtensions, &appInfo);

    if (m_enableValidation)
      vk_utils::initDebugReportCallback(m_instance, &debugReportCallbackFn, &m_debugReportCallback);

    volkLoadInstance(m_instance);

#ifndef VK_VERSION_1_2
    // NOTE: descriptor indexing is core in vulkan 1.2
    m_deviceExtensions.push_back(VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME);
#endif

    m_physicalDevice = vk_utils::findPhysicalDevice(m_instance, true, a_deviceId, m_deviceExtensions);

    // NOTE: Still need to enable device features.
    // Check this structure for other possibilities provided by descriptor indexing
    VkPhysicalDeviceDescriptorIndexingFeatures indexingFeatures{};
    indexingFeatures.sType= VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
    indexingFeatures.shaderSampledImageArrayNonUniformIndexing = VK_TRUE;
    indexingFeatures.runtimeDescriptorArray = VK_TRUE;

    m_pDeviceFeatures = &indexingFeatures;

    m_device = vk_utils::createLogicalDevice(m_physicalDevice, m_validationLayers, m_deviceExtensions,
                                             m_enabledDeviceFeatures, m_queueFamilyIDXs,
                                             VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT | VK_QUEUE_COMPUTE_BIT,
                                             m_pDeviceFeatures);

    vkGetDeviceQueue(m_device, m_queueFamilyIDXs.compute, 0, &m_queue);

    volkLoadDevice(m_device);

    m_commandPool = vk_utils::createCommandPool(m_device, m_queueFamilyIDXs.graphics, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
    m_pCopy = std::make_shared<vk_utils::PingPongCopyHelper>(m_physicalDevice, m_device,m_queue,
                                                             m_queueFamilyIDXs.compute, scratchMemSize);

      m_pAlloc = vk_utils::CreateMemoryAlloc_Simple(m_device, m_physicalDevice);
  }

  void InitBuffers()
  {
    m_fractalBufSize = WIDTH * HEIGHT * sizeof(Pixel);
    VkFlags fractalBufFlags = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    vk_utils::MemAllocInfo fractalAllocInfo {};
#if defined(USE_HOST_COHERENT_MEM)
    fractalAllocInfo.memProps = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
#else
    fractalAllocInfo.memUsage = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
#endif

    m_fractalBuffer = vk_utils::createBuffer(m_device, m_fractalBufSize, fractalBufFlags, &fractalAllocInfo.memReq);

    m_fractalBufferAllocID = m_pAlloc->Allocate(fractalAllocInfo);
    auto memBlock = m_pAlloc->GetMemoryBlock(m_fractalBufferAllocID);

    vkBindBufferMemory(m_device, m_fractalBuffer, memBlock.memory, memBlock.offset);
  }

  void InitTextures()
  {
    constexpr uint32_t WHITE  = 0x00FFFFFF;
    constexpr uint32_t BLACK  = 0x00000000;
    constexpr uint32_t RED    = 0x000000FF;
    constexpr uint32_t GREEN  = 0x0000FF00;
    constexpr uint32_t BLUE   = 0x00FF0000;
    constexpr uint32_t YELLOW = 0x0000FFFF;
    std::vector<uint32_t> red_black_white = {RED, BLACK, WHITE,
                                             BLACK, WHITE, RED,
                                             WHITE, RED, BLACK };
    std::vector<uint32_t> green_white = {GREEN, WHITE, WHITE, GREEN };
    std::vector<uint32_t> blue_cross  = {BLACK, BLACK, BLUE, BLUE, BLACK, BLACK,
                                         BLACK, BLACK, BLUE, BLUE, BLACK, BLACK,
                                         BLUE, BLUE, BLUE, BLUE, BLUE, BLUE,
                                         BLUE, BLUE, BLUE, BLUE, BLUE, BLUE,
                                         BLACK, BLACK, BLUE, BLUE, BLACK, BLACK,
                                         BLACK, BLACK, BLUE, BLUE, BLACK, BLACK,};
    std::vector<uint32_t> yellow_diamond = {BLACK, YELLOW, BLACK,
                                            YELLOW, BLACK, YELLOW,
                                            BLACK, YELLOW, BLACK };

    VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;
    VkFlags flags = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;

    // NOTE: With descriptor indexing we can use array of textures of different sizes
    m_images.push_back(vk_utils::createVkImage(m_device, 3, 3, format, flags));
    m_images.push_back(vk_utils::createVkImage(m_device, 2, 2, format, flags));
    m_images.push_back(vk_utils::createVkImage(m_device, 6, 6, format, flags));
    m_images.push_back(vk_utils::createVkImage(m_device, 3, 3, format, flags));

    vk_utils::MemAllocInfo allocInfo{};
    allocInfo.memUsage = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    m_texAlloc = m_pAlloc->Allocate(allocInfo, m_images);

    m_pCopy->UpdateImage(m_images[0], red_black_white.data(), 3, 3, 4, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    m_pCopy->UpdateImage(m_images[1], green_white.data(),     2, 2, 4, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    m_pCopy->UpdateImage(m_images[2], blue_cross.data(),      6, 6, 4, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    m_pCopy->UpdateImage(m_images[3], yellow_diamond.data(),  3, 3, 4, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    m_imageViews.push_back(vk_utils::createVkImageView(m_device, m_images[0], format));
    m_imageViews.push_back(vk_utils::createVkImageView(m_device, m_images[1], format));
    m_imageViews.push_back(vk_utils::createVkImageView(m_device, m_images[2], format));
    m_imageViews.push_back(vk_utils::createVkImageView(m_device, m_images[3], format));

    auto sampler = vk_utils::createSampler(m_device, VK_FILTER_NEAREST);
    m_samplers.resize(4);
    m_samplers.assign({sampler, sampler, sampler, sampler});
  }

  void CreatePipeline()
  {
    std::vector<std::pair<VkDescriptorType, uint32_t> > dtypes = {
        {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, m_imageViews.size()},
        {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1}
    };
    m_pBindings = std::make_shared<vk_utils::DescriptorMaker>(m_device, dtypes, 1);

    VkShaderStageFlags flags = VK_SHADER_STAGE_COMPUTE_BIT;
    m_pBindings->BindBegin(flags);
    m_pBindings->BindBuffer(0, m_fractalBuffer);
    // NOTE: here we bind VkImageView and VkSampler arrays to a single location
    m_pBindings->BindImageArray(1, m_imageViews, m_samplers);
    m_pBindings->BindEnd(&m_dSet, &m_dSetLayout);

    vk_utils::ComputePipelineMaker maker;
    maker.LoadShader(m_device, "../shaders/comp.spv");
    m_pipelineLayout = maker.MakeLayout(m_device, {m_dSetLayout}, sizeof(Offsets_T));
    m_pipeline =  maker.MakePipeline(m_device);
  }

  void SaveRenderedImage()
  {

    float* data_ptr = nullptr;
#if defined(USE_HOST_COHERENT_MEM)
    data_ptr = (float*)m_pAlloc->Map(m_fractalBufferAllocID, 0, VK_WHOLE_SIZE);
#else
    std::vector<float> image_float;
    image_float.resize(WIDTH * HEIGHT * 4);
    m_pCopy->ReadBuffer(m_fractalBuffer, 0, image_float.data(), m_fractalBufSize);
    data_ptr = image_float.data();
#endif

    std::vector<uint32_t> image_LDR(WIDTH * HEIGHT * 4, 0);
    for (int i = 0; i < HEIGHT; ++i)
    {
      for (int j = 0; j < WIDTH; ++j)
      {
        uint32_t base_idx = j + i * WIDTH;

        uint32_t R = static_cast<unsigned char>(255.0f * data_ptr[base_idx * 4 + 0]);
        uint32_t G = static_cast<unsigned char>(255.0f * data_ptr[base_idx * 4 + 1]) << 8;
        uint32_t B = static_cast<unsigned char>(255.0f * data_ptr[base_idx * 4 + 2]) << 16;
        uint32_t A = static_cast<unsigned char>(255.0f * data_ptr[base_idx * 4 + 3]) << 24;

        uint32_t val = R | G | B | A;

        image_LDR[base_idx] = val;
      }
    }

    SaveBMP("mandelbrot.bmp", image_LDR.data(), WIDTH, HEIGHT);
  }

  void RecordCommands(VkCommandBuffer a_cmdBuff, uint32_t xWork, uint32_t xOffset, uint32_t yWork, uint32_t yOffset)
  {
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    VK_CHECK_RESULT(vkBeginCommandBuffer(a_cmdBuff, &beginInfo));

    Offsets_T pcData {xOffset, yOffset};
    vkCmdPushConstants(a_cmdBuff, m_pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(pcData), &pcData);

    vkCmdBindPipeline(a_cmdBuff, VK_PIPELINE_BIND_POINT_COMPUTE, m_pipeline);
    vkCmdBindDescriptorSets(a_cmdBuff, VK_PIPELINE_BIND_POINT_COMPUTE, m_pipelineLayout, 0, 1, &m_dSet, 0, nullptr);

    vkCmdDispatch(a_cmdBuff, (xWork + WORKGROUP_SIZE - 1) / WORKGROUP_SIZE,
                             (yWork + WORKGROUP_SIZE - 1) / WORKGROUP_SIZE,
                             1);

    VK_CHECK_RESULT(vkEndCommandBuffer(a_cmdBuff));
  }

  void Cleanup()
  {
    vkDestroyBuffer(m_device, m_fractalBuffer, nullptr);

    for(auto& img : m_images)
      vkDestroyImage(m_device, img, nullptr);

    for(auto& view : m_imageViews)
      vkDestroyImageView(m_device, view, nullptr);

    // assuming all textures use the same sampler
    vkDestroySampler(m_device, m_samplers[0], nullptr);

    m_pAlloc->FreeAllMemory();
    m_pAlloc = nullptr;

    vkDestroyPipelineLayout(m_device, m_pipelineLayout, nullptr);
    vkDestroyPipeline(m_device, m_pipeline, nullptr);

    m_pCopy = nullptr;
    m_pBindings = nullptr;

    vkDestroyCommandPool(m_device, m_commandPool, nullptr);

    vkDestroyDevice(m_device, nullptr);

    if (m_enableValidation)
    {
      vkDestroyDebugReportCallbackEXT(m_instance, m_debugReportCallback, nullptr);
    }
    vkDestroyInstance(m_instance, nullptr);
  }

  static VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallbackFn(
      VkDebugReportFlagsEXT                       flags,
      VkDebugReportObjectTypeEXT                  objectType,
      uint64_t                                    object,
      size_t                                      location,
      int32_t                                     messageCode,
      const char* pLayerPrefix,
      const char* pMessage,
      void* pUserData)
  {
    std::cout << pLayerPrefix << " " << pMessage << std::endl;
    return VK_FALSE;
  }

public:
  void Run(uint8_t a_deviceId = 0)
  {
    std::cout << "init vulkan for device " << a_deviceId << " ... " << std::endl;
    InitVulkan(a_deviceId);
    InitBuffers();
    InitTextures();
    CreatePipeline();

    // we render the resulting image by splitting it into square tiles
    constexpr unsigned perTileX = TILE_X;
    constexpr unsigned perTileY = TILE_Y;
    constexpr unsigned nTilesX  = WIDTH / perTileX;
    constexpr unsigned nTilesY  = HEIGHT / perTileY;
    constexpr unsigned nTiles   = nTilesX * nTilesY;

    std::vector<VkCommandBuffer> cmdBuffers = vk_utils::createCommandBuffers(m_device, m_commandPool, nTiles);
    size_t idx = 0;
    for(size_t i = 0; i < nTilesY; ++i)
    {
      for(size_t j = 0; j < nTilesX; ++j)
      {
        RecordCommands(cmdBuffers[idx], perTileX, perTileX * j, perTileY, perTileY * i);
        idx++;
      }
    }

    std::cout << "Doing computations ... " << std::endl;

    VkFence fence;
    VkFenceCreateInfo fenceCreateInfo = {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.flags = 0;
    VK_CHECK_RESULT(vkCreateFence(m_device, &fenceCreateInfo, nullptr, &fence));

    {
      VkSubmitInfo submitInfo = {};
      submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
      submitInfo.commandBufferCount = cmdBuffers.size();
      submitInfo.pCommandBuffers = cmdBuffers.data();
      VK_CHECK_RESULT(vkQueueSubmit(m_queue, 1, &submitInfo, fence));
    }
    VK_CHECK_RESULT(vkWaitForFences(m_device, 1, &fence, VK_TRUE, vk_utils::DEFAULT_TIMEOUT));

    vkDestroyFence(m_device, fence, nullptr);
    vkFreeCommandBuffers(m_device, m_commandPool, cmdBuffers.size(), cmdBuffers.data());

    std::cout << "Saving image       ... " << std::endl;
    SaveRenderedImage();
    std::cout << "destroying all     ... " << std::endl;

    Cleanup();
  }
};

int main()
{
  ComputeApplication app;

  constexpr unsigned deviceId = 0;
  app.Run(deviceId);

  return 0;
}
