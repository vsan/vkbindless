# Vulkan bindless sample 
 
Demonstrates non-uniform descriptor indexing. 

Search for "NOTE" in project code to find specific places where the bindless approach is set up and
some useful information about it. 

![](image.png)

# Demo

The application launches a compute shader that renders the mandelbrot set into storage buffer while dynamically 
addressing array of textures to get the color for the visualization. 
The storage buffer is then read from the GPU, and saved as `mandelbrot.bmp`. 

# Building

Clone external dependencies by running ["clone_dependecies.bat"](clone_dependecies.bat).
Build as any CMake project, for example:
```shell
mkdir build
cd build
cmake ..
make
```

Or just open in your favourite IDE which supports CMake projects.