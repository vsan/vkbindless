#ifndef VK_BINDLESS_SHADERCOMMON_H
#define VK_BINDLESS_SHADERCOMMON_H

#define WIDTH 2048
#define HEIGHT 2048
#define WORKGROUP_SIZE 16

#define TILE_X 128
#define TILE_Y 128

#ifdef __cplusplus
typedef unsigned int uint;
#endif

struct Offsets_T
{
  uint offsetX;
  uint offsetY;
};

const uint FRACTAL_ITERATIONS = 256;

#endif //VK_BINDLESS_SHADERCOMMON_H
