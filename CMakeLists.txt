cmake_minimum_required (VERSION 3.8)
project (vk_bindless)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)

add_library(project_warnings INTERFACE)
include(cmake/CompilerWarnings.cmake)
set_project_warnings(project_warnings)

add_compile_definitions(USE_VOLK)
add_definitions(-D_CRT_SECURE_NO_WARNINGS)

##############################################

if(CMAKE_SYSTEM_NAME STREQUAL Windows)
    set(VOLK_STATIC_DEFINES VK_USE_PLATFORM_WIN32_KHR)
elseif(CMAKE_SYSTEM_NAME STREQUAL Linux)
    set(VOLK_STATIC_DEFINES VK_USE_PLATFORM_XLIB_KHR)
elseif(CMAKE_SYSTEM_NAME STREQUAL Darwin)
    set(VOLK_STATIC_DEFINES VK_USE_PLATFORM_MACOS_MVK)
endif()

include_directories(${CMAKE_SOURCE_DIR}/external/volk)
include_directories(${CMAKE_SOURCE_DIR}/external/vkutils)

set(VK_UTILS_SRC
        ${CMAKE_SOURCE_DIR}/external/vkutils/external/samplers_vk.cpp
        ${CMAKE_SOURCE_DIR}/external/vkutils/vk_alloc_vma.cpp
        ${CMAKE_SOURCE_DIR}/external/vkutils/vk_alloc_simple.cpp
        ${CMAKE_SOURCE_DIR}/external/vkutils/vk_copy.cpp
        ${CMAKE_SOURCE_DIR}/external/vkutils/vk_utils.cpp
        ${CMAKE_SOURCE_DIR}/external/vkutils/vk_buffers.cpp
        ${CMAKE_SOURCE_DIR}/external/vkutils/vk_images.cpp
        ${CMAKE_SOURCE_DIR}/external/vkutils/vk_pipeline.cpp
        ${CMAKE_SOURCE_DIR}/external/vkutils/vk_descriptor_sets.cpp)


##############################################

add_subdirectory(external/volk)
add_subdirectory(src)